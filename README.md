# JS COMMON APPLICATION PATTERNS
### This is a curated list of the most common javascript patterns to use in an application, along with it's examples.
---

#### 1). Observer pattern
As from [here, ](https://addyosmani.com/resources/essentialjsdesignpatterns/book/#observerpatternjavascript)
"
is a **design pattern** where an object (known as a **subject**) maintains a list of objects depending on it (**observers**), automatically notifying them of any changes to state.

When a subject needs to **notify** observers about something interesting happening, it broadcasts a notification to the observers (which can include specific data related to the topic of the notification).
When we no longer wish for a particular observer to be notified of changes by the subject they are registered with, the subject can remove them from the list of observers."

[example:](../observer.pattern.js) 
