/*
 Vanilla Implementation of the observer and Module Pattern.
 This also serves as an example of global Imports, using SubjectTypes global.

 The Observer is a design pattern where an object (known as a subject) maintains a list of 
 objects depending on it (observers), automatically notifying them of any changes to state.

 When a subject needs to notify observers about something interesting happening, 
 it broadcasts a notification to the observers 
 (which can include specific data related to the topic of the notification).

 FROM: https://addyosmani.com/resources/essentialjsdesignpatterns/book/#observerpatternjavascript
 */

var SubjectTypes = ['action1','action2','action2'];

var Subject = (function(SubjectTypes) {
  var observers = [];
  
  // suscribe observers
  function suscribe(observer) {
  	if (this.observers === undefined) {
    	this.observers = [];
    }
    
    if (observer !== undefined) {
    	this.observers.push(observer);
    }
  }
  
  // unsuscribe observers
  function unsuscribe(observer) {
    if (observer !== undefined) {
      var observerIndex = this.observers.indexOf(observer);

      if (observerIndex > -1) {
        this.observers.splice(observerIndex, 1);
      }

    }

  }
   
  // notify function to alert all observers
  function notify() {

    if (this.observers.length > 0) {
      var i;
      for (i = 0; i < this.observers.length; i ++) {
        var observerFunction = this.observers[i];
        observerFunction();
      }
    }

  }
  
  // count the number of observers subscribed
  function count() {
  	return this.observers.length;
  }

  // clear all observers registered.
  function clearObservers() {
    this.observers = [];
  }
  
  // declare public attributes
  return {
    suscribe: suscribe,
    unsuscribe : unsuscribe,
    notify : notify,
    count : count,
    clearObservers: clearObservers
  }
  
})(SubjectTypes);

// Observer Pattern testing...
// here we declare various functions that will suscribe to the Subject observer...

var logHello = function() {
	console.info ("printing hey");
};

var logHi= function() {
	console.info ("printing hi");
};

var logbye = function() {
	console.info ("printing bye");
};


Subject.suscribe(logHello);
Subject.suscribe(logHi);
Subject.suscribe(logbye);

Subject.notify();

console.log("unsuscribing logbye observer...");
Subject.unsuscribe(logbye);

console.log("observers remaining = " + Subject.count() );

console.log("Now notifying all observers...");
Subject.notify();

console.log("clearing all observers...");
Subject.clearObservers();

console.log("observers remaining = " + Subject.count() );





